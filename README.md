Deployr
=======

Node app auto deployment tool using capistrano and pm2.

![destoroyah_01.png](https://bitbucket.org/repo/4qBMdq/images/1473631849-destoroyah_01.png)

## Description

- [Capistrano](http://capistranorb.com/)
- [PM2](http://pm2.keymetrics.io/)
- [Bundler](http://bundler.io/)
- [platanus/capistrano-npm](https://github.com/platanus/capistrano-npm)
- [ne1ro/capistrano_pm2](https://github.com/ne1ro/capistrano_pm2)


## Useage

### 1. ssh-agent

```bash
$ eval `ssh-agent -s`
$ ssh-add ~/.ssh/server_rsa
$ ssh-add ~/.ssh/repo_rsa
```


### 2. Bundle install

```bash
$ bundle install
```


### 3. Deploy

```bash
$ bundle exec cap production deploy

$ bundle exec cap production pm2:restart
```
