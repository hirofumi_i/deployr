# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'dewy'
set :repo_url, 'git@bitbucket.org:hirofumi_i/dewy.git'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/html/dewy/public_html'

# User
set :user, 'user'

# Default value for keep_releases is 5
set :keep_releases, 5

set :pm2_config, '/var/www/html/dewy/public_html/current/processes.json'

namespace :deploy do

    desc 'Restart application'
    task :restart do
        on roles(:app), in: :sequence, wait: 5 do
            info 'Restart'
        end
    end

    after :publishing, :restart

    before :restart, 'npm:install'

end
