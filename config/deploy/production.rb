server '127.0.0.1',
    user: 'user',
    roles: %w[app web db],
    ssh_options: {
        keys: [File.expand_path('~/.ssh/conoha_rsa')],
        port: 41527,
        forward_agent: true,
        auth_methods: %w(publickey)
    }
